local M = {}

function M.find_files()
	local opts = {}
	local telescope = require "telescope.builtin"
	telescope.find_files(opts)
end

return M
