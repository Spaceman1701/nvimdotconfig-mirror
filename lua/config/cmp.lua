local M = {}

function M.setup()
	local has_words_before = function()
		local line, col = unpack(vim.api.nvim_win_get_cursor(0))
		return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match "%s" == nil
	end
	local cmp = require "cmp"
	local lspkind = require "lspkind"
	local cmp_autopairs = require "nvim-autopairs.completion.cmp"
	local luasnip = require "luasnip"
	cmp.event:on("confirm_done", cmp_autopairs.on_confirm_done { map_char = { tex = "" } })
	vim.o.pumheight = 10
	cmp.setup {
		completion = { completeopt = "menu,menuone,noinsert,noselect", keyword_length = 1, },
		experimental = { native_menu = false, ghost_text = true },
		snippet = {
			expand = function(args)
				luasnip.lsp_expand(args.body)
			end,
		},
		formatting = {
			-- format = function(entry, vim_item)
			-- 	vim_item.menu = ({
			-- 		buffer = "[Buffer]",
			-- 		nvim_lua = "[Lua]",
			-- 		treesitter = "[Treesitter]",
			-- 	})[entry.source.name]
			-- return vim_item
			-- end,
			format = lspkind.cmp_format({
				mode = "symbol",
				maxwidth = 50,
				ellipsis_char = "...",
				before = function(entry, vim_item)
					vim_item.menu = ({
						buffer = "[Buffer]",
						nvim_lua = "[Lua]",
						treesitter = "[Treesitter]",
					})[entry.source.name]
					return vim_item
				end
			})
		},
		mapping = {
			["<C-k>"] = cmp.mapping(cmp.mapping.select_prev_item(), { "i", "c" }),
			["<C-j>"] = cmp.mapping(cmp.mapping.select_next_item(), { "i", "c" }),
			["<C-b>"] = cmp.mapping(cmp.mapping.scroll_docs(-4), { "i", "c" }),
			["<C-f>"] = cmp.mapping(cmp.mapping.scroll_docs(4), { "i", "c" }),
			["<C-Space>"] = cmp.mapping(function()
				if cmp.visible() then
					cmp.close()
				else
					cmp.complete()
				end
			end, { "i", "c" }),
			["<C-e>"] = cmp.mapping { i = cmp.mapping.close(), c = cmp.mapping.close() },
			["<CR>"] = cmp.mapping(cmp.mapping.confirm({ behavior = cmp.ConfirmBehavior.Insert, select = false }),
				{ "i", "c" }),
			["<Tab>"] = cmp.mapping(function(fallback)
				if cmp.visible() then
					cmp.select_next_item()
				elseif luasnip.expand_or_jumpable() then
					luasnip.expand_or_jump()
				elseif has_words_before() then
					cmp.complete()
				else
					fallback()
				end
			end, { "i", "s", "c" }),
			["<S-Tab>"] = cmp.mapping(function(fallback)
				if cmp.visible() then
					cmp.select_prev_item()
				else
					fallback()
				end
			end,
				{ "i", "s", "c" }),
		},

		sources = {
			{ name = "luasnip" },
			{ name = "nvim_lsp" },
			{ name = "treesitter" },
			{ name = "buffer" },
			{ name = "nvim_lua" },
			{ name = "path" },
			{ name = "nvim_lsp_signature_help" },
		},
		--[[ documentation = {
      border = { "╭", "─", "╮", "│", "╯", "─", "╰", "│" },
      winhighlight = "NormalFloat:NormalFloat,FloatBorder:TelescopeBorder",
    }, ]]
	}
	cmp.setup.cmdline("/", {
		sources = {
			{ name = "buffer" },
		},
	})

	cmp.setup.cmdline(":", {
		sources = cmp.config.sources({
			{ name = "path" },
		}, {
			{ name = "cmdline" },
		}),
	})
end

return M
