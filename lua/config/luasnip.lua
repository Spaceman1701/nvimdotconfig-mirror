local M = {}

function M.setup()
  local luasnip = require "luasnip"

  luasnip.config.set_config {
    history = false,
    updateevents = "TextChanged,TextChangedI",
		region_check_events = 'InsertEnter',
		delete_check_events = 'InsertLeave',
  }

  require("luasnip/loaders/from_vscode").load()
end

return M
