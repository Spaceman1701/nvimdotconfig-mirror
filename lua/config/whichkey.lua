local M = {}

function M.setup()
	local whichkey = require "which-key"

	local conf = {
		window = {
			border = "single",
			position = "bottom",
		},
	}

	local opts = {
		mode = "n",
		prefix = "<leader>",
		buffer = nil,
		silent = true,
		noremap = true,
		nowait = false,
	}


	local keymaps_fzf = {
		name = "Find",
		f = { "<cmd>lua require('utils.finder').find_files()<cr>", "Files" },
		b = { "<cmd>Telescope buffers<cr>", "Buffers"},
		o = { "<cmd>Telescope oldfiles<cr>", "Old Lives"},
		g = { "<cmd>Telescope live_grep<cr>", "Live Grep"},
		c = { "<cmd>Telescope commands<cr>", "Commands"},
		r = { "<cmd>Telescope file_browser<cr>", "File Browser" },
		w = { "<cmd>Telescope current_buffer<cr>", "Current Buffer"},
		d = { "<cmd>Telescope diagnostics<cr>", "LSP Diagnostics"},
		s = { "<cmd>Telescope lsp_workspace_symbols<cr>", "LSP Symbols"},
		R = { "<cmd>Telescope lsp_references<cr>", "LSP References"},
	}

	local keymaps_project = {
		name = "Project",
		p = { "<cmd>lua require'telescope'.extensions.project.project{}<cr>", "List" },
    s = { "<cmd>Telescope repo list<cr>", "Search" },
	}

	local mappings = {
		["w"] = { "<cmd>update!<CR>", "Save" },
		["j"] = { "<cmd>HopWord<cr>", "Hop" },
		b = {
			name = "Buffer",
			q = {"<Cmd>bd<Cr>", "Close Buffer"},
		},
		f = keymaps_fzf,
		p = keymaps_project,
	}
	whichkey.setup(conf)
	whichkey.register(mappings, opts)
end

return M
