local M = {}

function M.setup()
	local actions = require "telescope.actions"
	local telescope = require "telescope"
	
	telescope.load_extension "fzf"
	telescope.load_extension "project"
	telescope.load_extension "repo"
	telescope.load_extension "file_browser"
	telescope.load_extension "projects"
end

return M
