local M = {}
local servers = {
	rust_analyzer = {},
	sumneko_lua = {},
	gopls = {},
	pyright = {},
}

local function on_attach(client, bufnr)
	vim.api.nvim_buf_set_option(bufnr, "omnifunc", "v:lua.vim.lsp.omnifunc")
	vim.api.nvim_buf_set_option(0, "formatexpr", "v:lua.vim.lsp.formatexpr")
	require("config.lsp.keymaps").setup(client, bufnr)
end

local lsp_signature = require "lsp_signature"
lsp_signature.setup {
	bind = true,
	handler_opts = {
		border = "rounded",
	},
}

local capabilities = require("cmp_nvim_lsp").default_capabilities()

local opts = {
	on_attach = on_attach,
	capabilities = capabilities,
	flags = {
		debounce_text_changes = 150,
	},
}

require("config.lsp.handlers").setup()

function M.setup() 
	require("config.lsp.installer").setup(servers, opts)
end

return M
