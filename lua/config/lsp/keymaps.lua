local M = {}

local whichkey = require "which-key"

local keymap = vim.api.nvim_set_keymap
local buf_keymap = vim.api.nvim_buf_set_keymap

local function keymappings(client, bufnr)
	local opts = { noremap = true, silent = true }

	buf_keymap(bufnr, "n", "K", "<cmd>lua vim.lsp.buf.hover()<CR>", opts)
	keymap("n", "[d", "<cmd>lua vim.diagnostic.goto_prev()<CR>", opts)
	keymap("n", "]d", "<cmd>lua vim.diagnostic.goto_next()<CR>", opts)
	keymap("n", "[e", "<cmd>lua vim.diagnostic.goto_prev({severity = vim.diagnostic.severity.ERROR})<CR>", opts)
  keymap("n", "]e", "<cmd>lua vim.diagnostic.goto_next({severity = vim.diagnostic.severity.ERROR})<CR>", opts)

	local keymap_l = {
		name = "LSP Actions",
		r = { "<cmd>lua vim.lsp.buf.rename()<CR>", "Rename" },
	  a = { "<cmd>lua vim.lsp.buf.code_action()<CR>", "Code Action" },
	  i = { "<cmd>lua vim.diagnostic.open_float()<CR>", "Line Diagnostics" },
		d = { "<Cmd>lua vim.lsp.buf.definition()<CR>", "Definition" },
		D = { "<Cmd>lua vim.lsp.buf.declaration()<CR>", "Declaration" },
		s = { "<cmd>lua vim.lsp.buf.signature_help()<CR>", "Signature Help" },
		I = { "<cmd>lua vim.lsp.buf.implementation()<CR>", "Goto Implementation" },
		t = { "<cmd>lua vim.lsp.buf.type_definition()<CR>", "Goto Type Definition" },
		f = { "<cmd>lua vim.lsp.buf.format()<CR>", "Format Buffer"},
	}

	whichkey.register(keymap_l, { buffer = bufnr, prefix = "<leader>c"})
end

function M.setup(client, bufnr) 
 keymappings(client, bufnr)
end

return M
